
// opencvhw2Dlg.cpp : 實作檔
//

#include "stdafx.h"
#include "opencvhw2.h"
#include "opencvhw2Dlg.h"
#include "afxdialogex.h"
#include "opencv2\highgui\highgui.hpp"
#include "opencv\cv.h"
#include "stdio.h"

#include <vector>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace cv;
using namespace std;

// 對 App About 使用 CAboutDlg 對話方塊

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 對話方塊資料
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支援

// 程式碼實作
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Copencvhw2Dlg 對話方塊



Copencvhw2Dlg::Copencvhw2Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Copencvhw2Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Copencvhw2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Copencvhw2Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON3, &Copencvhw2Dlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON1, &Copencvhw2Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &Copencvhw2Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON4, &Copencvhw2Dlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &Copencvhw2Dlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &Copencvhw2Dlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &Copencvhw2Dlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &Copencvhw2Dlg::OnBnClickedButton8)
END_MESSAGE_MAP()


// Copencvhw2Dlg 訊息處理常式

BOOL Copencvhw2Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 將 [關於...] 功能表加入系統功能表。

	// IDM_ABOUTBOX 必須在系統命令範圍之中。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 設定此對話方塊的圖示。當應用程式的主視窗不是對話方塊時，
	// 框架會自動從事此作業
	SetIcon(m_hIcon, TRUE);			// 設定大圖示
	SetIcon(m_hIcon, FALSE);		// 設定小圖示

	// TODO:  在此加入額外的初始設定
	AllocConsole();
	freopen("CONOUT$", "w", stdout);



	return TRUE;  // 傳回 TRUE，除非您對控制項設定焦點
}

void Copencvhw2Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果將最小化按鈕加入您的對話方塊，您需要下列的程式碼，
// 以便繪製圖示。對於使用文件/檢視模式的 MFC 應用程式，
// 框架會自動完成此作業。

void Copencvhw2Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 繪製的裝置內容

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 將圖示置中於用戶端矩形
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 描繪圖示
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 當使用者拖曳最小化視窗時，
// 系統呼叫這個功能取得游標顯示。
HCURSOR Copencvhw2Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void Copencvhw2Dlg::OnBnClickedButton1() //1-1
{
	// TODO:  在此加入控制項告知處理常式程式碼

	IplImage* img = cvLoadImage("./Eyes.png");
	IplImage* dst = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,3);
	cvNamedWindow("Eye_smooth", CV_WINDOW_AUTOSIZE);
	
	
	cvSmooth(img, dst, CV_MEDIAN,5,5);
	
	cvShowImage("Eye_smooth", dst);
	cvWaitKey(0);
	cvReleaseImage(&img);
	cvReleaseImage(&dst);
	cvDestroyWindow("Eye_smooth");


}


void Copencvhw2Dlg::OnBnClickedButton2() //1-2
{
	// TODO:  在此加入控制項告知處理常式程式碼
	IplImage* img = cvLoadImage("./Eyes.png");
	IplImage* smo = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 3);
	IplImage* can = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);
	cvNamedWindow("Eye_canny", CV_WINDOW_AUTOSIZE);
	
	
	cvSmooth(img, smo, CV_MEDIAN,5,5);
	
	cvCanny(smo, can, 50, 100, 3);


	cvShowImage("Eye_canny", can);
	cvWaitKey(0);
	cvReleaseImage(&img);
	cvReleaseImage(&smo);
	cvReleaseImage(&can);
	cvDestroyWindow("Eye_canny");



}


void Copencvhw2Dlg::OnBnClickedButton3() //1-3
{
	// TODO:  在此加入控制項告知處理常式程式碼
	IplImage* img = cvLoadImage("./Eyes.png",CV_LOAD_IMAGE_GRAYSCALE);
	IplImage* smo = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);
	IplImage* cir = cvLoadImage("./Eyes.png");
	cvNamedWindow("Eye_circle", CV_WINDOW_AUTOSIZE);

	
	cvSmooth(img, smo, CV_MEDIAN, 5, 5);
	cvSmooth(cir, cir, CV_MEDIAN, 5, 5);
	
	CvMemStorage *storage = cvCreateMemStorage(0);
	CvSeq* results = cvHoughCircles(smo, storage, CV_HOUGH_GRADIENT
		, 4, smo->width / 10);
	//因為需要計算像素梯度的方向，所以來源影像最好是灰階影像，二值化影像較難判斷
	//"2"表示累加器圖像的分辨率，不能小於1
	//"src->width/10"表示兩個圓之間的最小距離
	for (int i = 0; i < results->total; i++)
	{
		float* p = (float*)cvGetSeqElem(results, i);//將序列中的值取出
		CvPoint pt = cvPoint(cvRound(p[0]+1), cvRound(p[1] - p[2] / 2));
		cvCircle(cir, pt, cvRound(p[2]/3), CV_RGB(0xff, 0, 0));
		//cvCircle(來源影像, 圓心座標, 圓的半徑, 顏色)
	}
	



	cvShowImage("Eye_circle", cir);
	cvWaitKey(0);
	cvReleaseImage(&img);
	cvReleaseImage(&smo);
	cvDestroyWindow("Eye_circle");

}


void Copencvhw2Dlg::OnBnClickedButton4() //2-1
{
	// TODO:  在此加入控制項告知處理常式程式碼
	/*0~255 gray count*/
	/*
	Mat image = imread("./Eyes.png");
	
	cvtColor(image, image, CV_BGR2GRAY);

	cvNamedWindow("Eye_circle", CV_WINDOW_AUTOSIZE);

	int *columnv = new int[image.cols];

	for (int i = 0; i < image.cols; i++)
	{
		columnv[i] = 0;
		for (int j = 0; j < image.rows; j++)
		{
			
			columnv[i] += image.at<uchar>(j, i);
		}
		printf("%d ",columnv[i]);
	}
	
	imshow("Eye_circle", image);
	cvWaitKey(0);
	
	cvDestroyWindow("Eye_circle");
	*/
	Mat image = imread("./temple.png");

	cvtColor(image, image, CV_BGR2GRAY);

	cvNamedWindow("Temple_gau2", CV_WINDOW_AUTOSIZE);

	Mat gauss;

	GaussianBlur(image, gauss,cvSize(5,5),0,0);
	pyrDown(gauss, gauss);
	GaussianBlur(gauss, gauss, cvSize(5,5), 0,0);
	pyrDown(gauss, gauss);

	imshow("Temple_gau2", gauss);
	cvWaitKey(0);

	cvDestroyWindow("Temple_gau2");

}


void Copencvhw2Dlg::OnBnClickedButton5() //2-2
{
	// TODO:  在此加入控制項告知處理常式程式碼
	Mat image = imread("./temple.png");

	cvtColor(image, image, CV_BGR2GRAY);

	cvNamedWindow("Temple_lap2", CV_WINDOW_AUTOSIZE);

	Mat gauss1,gauss2,lapla2;

	GaussianBlur(image, gauss1, cvSize(5, 5), 0, 0);
	pyrDown(gauss1, gauss1);
	GaussianBlur(gauss1, gauss2, cvSize(5, 5), 0, 0);
	pyrDown(gauss2, gauss2);
	pyrUp(gauss2, lapla2);
	GaussianBlur(lapla2, lapla2, cvSize(5, 5), 0, 0);
	
	lapla2.copySize(gauss1);
	lapla2 = gauss1 - lapla2;
	
	imshow("Temple_lap2", lapla2);
	cvWaitKey(0);

	cvDestroyWindow("Temple_lap2");
}


void Copencvhw2Dlg::OnBnClickedButton6() //2-3
{
	// TODO:  在此加入控制項告知處理常式程式碼
	Mat image = imread("./temple.png");

	cvtColor(image, image, CV_BGR2GRAY);

	cvNamedWindow("Temple_lap1", CV_WINDOW_AUTOSIZE);

	Mat gauss1, gauss2, lapla2, lapla1;


	GaussianBlur(image, gauss1, cvSize(5, 5), 0, 0);
	pyrDown(gauss1, gauss1);
	GaussianBlur(gauss1, gauss2, cvSize(5, 5), 0, 0);
	pyrDown(gauss2, gauss2);
	pyrUp(gauss2, lapla2);
	GaussianBlur(lapla2, lapla2, cvSize(5, 5), 0, 0);
	pyrUp(gauss1, lapla1);
	GaussianBlur(lapla1, lapla1, cvSize(5, 5), 0, 0);

	lapla1.copySize(image);
	lapla1= image - lapla1;
	

	imshow("Temple_lap1", lapla1);
	cvWaitKey(0);

	cvDestroyWindow("Temple_lap1");
}


void Copencvhw2Dlg::OnBnClickedButton7() //2-4
{
	// TODO:  在此加入控制項告知處理常式程式碼
	Mat image = imread("./temple.png");

	cvtColor(image, image, CV_BGR2GRAY);

	cvNamedWindow("Temple_result", CV_WINDOW_AUTOSIZE);

	Mat gauss1, gauss2, lapla2, lapla1,lapla2t,lapla1t;

	GaussianBlur(image, gauss1, cvSize(5, 5), 0);
	pyrDown(gauss1, gauss1);
	GaussianBlur(gauss1, gauss2, cvSize(5, 5), 0);
	pyrDown(gauss2, gauss2);
	pyrUp(gauss2, lapla2);
	GaussianBlur(lapla2, lapla2, cvSize(5, 5), 0);
	pyrUp(gauss1, lapla1);
	GaussianBlur(lapla1, lapla1, cvSize(5, 5), 0);
	
	lapla2t = lapla2.clone();
	lapla1t = lapla1.clone();

	lapla1.copySize(image);
	lapla1 = image - lapla1 ;
	
	lapla2.copySize(gauss1);
	lapla2 = gauss1 - lapla2 ;
	
	lapla2t.copySize(lapla2);
	lapla2 = lapla2 + lapla2t ;
	
	pyrUp(lapla2, lapla1t);
	
	lapla1t.copySize(lapla1);
	lapla1 = lapla1 + lapla1t ;
	


	imshow("Temple_result", lapla1);
	cvWaitKey(0);

	cvDestroyWindow("Temple_result");
}


void Copencvhw2Dlg::OnBnClickedButton8() //2-5
{
	// TODO:  在此加入控制項告知處理常式程式碼

	Mat image = imread("./temple.png");

	cvtColor(image, image, CV_BGR2GRAY);

	

	Mat gauss1, gauss2, lapla2, lapla1, lapla2t, lapla1t;

	GaussianBlur(image, gauss1, cvSize(5, 5), 0);
	pyrDown(gauss1, gauss1);
	GaussianBlur(gauss1, gauss2, cvSize(5, 5), 0);
	pyrDown(gauss2, gauss2);
	pyrUp(gauss2, lapla2);
	GaussianBlur(lapla2, lapla2, cvSize(5, 5), 0);
	pyrUp(gauss1, lapla1);
	GaussianBlur(lapla1, lapla1, cvSize(5, 5), 0);

	lapla2t = lapla2.clone();
	lapla1t = lapla1.clone();

	lapla1.copySize(image);
	lapla1 = image - lapla1;

	lapla2.copySize(gauss1);
	lapla2 = gauss1 - lapla2;

	lapla2t.copySize(lapla2);
	lapla2 = lapla2 + lapla2t;

	pyrUp(lapla2, lapla1t);

	lapla1t.copySize(lapla1);
	lapla1 = lapla1 + lapla1t;

	double avg_error=0;

	for (int i = 0; i < lapla1.rows; i++)
	{
		for (int j = 0; j < lapla1.cols;j++)
		{
			avg_error += image.at<uchar>(i, j) - lapla1.at<uchar>(i, j);
		}
	}

	avg_error = abs(avg_error / (lapla1.cols * lapla1.rows));
	printf("average error per pixel: %03.3llf", avg_error);

}
